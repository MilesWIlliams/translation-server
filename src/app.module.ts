import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TranslationsModule } from './features/translations/translations.module';
import { App } from './config/app.config';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb+srv://${App.db_username}:${App.db_username_password}@gli-translations.al9ab.mongodb.net`, {useNewUrlParser: true, dbName: App.db_name, useFindAndModify: false, retryAttempts: 100, socketTimeoutMS: 30000, keepAlive: true }),
    TranslationsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}