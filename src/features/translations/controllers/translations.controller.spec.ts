// import { Test, TestingModule } from '@nestjs/testing';
// import { TranslationsController } from './translations.controller';

// describe('Translations Controller', () => {
//   let controller: TranslationsController;

//   beforeEach(async () => {
//     const module: TestingModule = await Test.createTestingModule({
//       controllers: [TranslationsController],
//     }).compile();

//     controller = module.get<TranslationsController>(TranslationsController);
//   });

//   it('should be defined', () => {
//     expect(controller).toBeDefined();
//   });
// });


import { Test } from '@nestjs/testing';
import { TranslationsController } from './translations.controller';
import { TranslationsService } from './../services/translations.service';

describe('TranslationsController', () => {
  let translationsController: TranslationsController;
  let translationService: TranslationsService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
        controllers: [TranslationsController],
        providers: [TranslationsService],
      }).compile();

      translationService = module.get<TranslationsService>(TranslationsService);
    translationsController = module.get<TranslationsController>(TranslationsController);
  });

  // describe('findAll', () => {
  //   it('should return an array of cats', async () => {
  //     const result = ['test'];
  //     jest.spyOn(translationService, 'getAllTranslations').mockImplementation(() => result);

  //     expect(await translationsController.getAllTranslations()).toBe(result);
  //   });
  // });
});