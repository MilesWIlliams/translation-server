import { Controller, Get, Res, HttpStatus, Post, Body, Delete, NotFoundException, Query, Param, Put, Patch } from '@nestjs/common';
import { TranslationsService } from '../services/translations.service';
import { Translation, TranslationFilter, TranslationFilterBodyMOdel } from '../interfaces/translation.interface';
import { CreateTranslationDTO, InsertColumnRequestDTO, InsertRowRequestDTO } from '../dto/create-translation.dto';
import { ApiImplicitParam, ApiImplicitQuery, ApiImplicitBody } from '@nestjs/swagger';


@Controller('translations')
export class TranslationsController {
    constructor(private _translationSvc: TranslationsService) {}

    @Get('')
    async getAllTranslations(@Res() res) {
        const translations = await this._translationSvc.getAllTranslations();
        return res.status(HttpStatus.OK).json({
            message: "Success",
            data: translations
        });
    }

    @Get('/:id')
    @ApiImplicitParam({ name: 'id' })
    async getTransactionByID(@Res() res, @Param('id') id) {
        const translation =  await this._translationSvc.getByID(id);
        return res.status(HttpStatus.OK).json({
            message: "Success",
            data: translation
        });

    }

    @Post('/create')
    async createTranslation(@Res() res, @Body() createTranslationDTO: CreateTranslationDTO) {
        const newTranslation =  await this._translationSvc.addTranslation(createTranslationDTO);
        return res.status(HttpStatus.OK).json({
            message: "Translation has been created successfully",
            data: newTranslation
        });
    }

    @Put('/:id')
    @ApiImplicitParam({ name: 'id' })
    async updateTranslationByID(@Res() resizeBy, @Param('id') id, @Body() payload: Translation) {
        const updatedTranslation = await this._translationSvc.updateByID(id, payload);

        return resizeBy.status(HttpStatus.OK).json({
            message: "Successfully updated translation",
            data: updatedTranslation
        });
    }

    @Delete('/delete')
    @ApiImplicitQuery({name: 'translationID'})
    async deleteTranslation(@Res()  res, @Query('translationID') translationID) {
        const translation = await this._translationSvc.deleteTranslation(translationID);

        if (!translation) throw new NotFoundException('Translation does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Translation has been deleted',
            data: translation
        })
    }

    @Post('filter')
    @ApiImplicitBody({name: 'body', type: TranslationFilterBodyMOdel})
    async filterTranslations(@Res()  res, @Body() query: TranslationFilter) {
        const translations = await this._translationSvc.filterTranslations(query.query);

        if (!translations) throw new NotFoundException('Translation does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Success',
            data: translations
        })
    }

    @Post('/insert/column')
    async insertNewColumn(@Res() res, @Body() createTranslationDTO: InsertColumnRequestDTO) {
        const newTranslation =  await this._translationSvc.insertColumn(createTranslationDTO);
        return res.status(HttpStatus.OK).json({
            message: "Translation column has been created successfully",
            data: newTranslation
        });
    }

    @Post('/insert/row')
    async insertNewRow(@Res() res, @Body() createTranslationDTO: InsertRowRequestDTO) {
        const newTranslation =  await this._translationSvc.insertRow(createTranslationDTO);
        return res.status(HttpStatus.OK).json({
            message: "Translation row has been created successfully",
            data: newTranslation
        });
    }

    @Post('/insert/update')
    async importAndUpdate(@Res() res, @Body() createTranslationDTO: Translation[]) {
        const newTranslation =  await this._translationSvc.importAndUpdate(createTranslationDTO);
        const translations = await this._translationSvc.getAllTranslations();
        return res.status(HttpStatus.OK).json({
            message: "Success",
            data: translations,
            count: newTranslation 
        });
    }

    @Post('/insert/replace')
    async importAndReplace(@Res() res, @Body() createTranslationDTO: CreateTranslationDTO[]) {
        const newTranslation =  await this._translationSvc.importAndReplace(createTranslationDTO);
        return res.status(HttpStatus.OK).json({
            message: "Translation row has been created successfully",
            data: newTranslation
        });
    }
}
