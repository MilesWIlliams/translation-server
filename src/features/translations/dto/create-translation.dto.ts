import { ApiModelProperty } from "@nestjs/swagger";

export class CreateTranslationDTO {
    @ApiModelProperty()
    readonly key: string;

    @ApiModelProperty()
    readonly language: string;

    @ApiModelProperty()
    readonly string: string;

    @ApiModelProperty()
    readonly description: string;
}

export class InsertColumnRequestDTO {
    @ApiModelProperty()
    readonly language: string;
}

export class InsertRowRequestDTO {
    @ApiModelProperty()
    readonly key: string;

    @ApiModelProperty()
    readonly description?: string;
}