import { Document } from "mongoose";

export interface Translation extends Document {
    readonly key?: string;
    readonly language: string;
    readonly string?: string;
    readonly description?: string;
}

export interface TranslationFilter {
    query: string;
}

export class TranslationFilterBodyMOdel {
    query: string;
}