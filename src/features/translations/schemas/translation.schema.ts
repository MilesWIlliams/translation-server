import * as mongoose from 'mongoose';

export const TranslationSchema = new mongoose.Schema({
    key: String,
    language: String,
    string: String,
    description: String
});
