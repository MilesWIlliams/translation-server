import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Translation } from './../interfaces/translation.interface';
import { CreateTranslationDTO, InsertColumnRequestDTO, InsertRowRequestDTO } from '../dto/create-translation.dto';
import { isNull } from 'util';

@Injectable()
export class TranslationsService {
    constructor(@InjectModel('Translations') private readonly translationModel: Model<Translation>) {}

    async getAllTranslations(): Promise<Translation[]> {
        return await this.translationModel.find().exec();

    }

    async getByID(id: string): Promise<Translation> {
        return await this.translationModel.findById(id).exec();
    }

    async addTranslation(createTranslationDTO: CreateTranslationDTO): Promise<Translation> {
        const newTranslation = new this.translationModel(createTranslationDTO);
        return await newTranslation.save();
    }

    async importAndUpdate(translations: Translation[]): Promise<any> {

        const promises = [];
        translations.forEach(t => {
            if (t._id) {
                return promises.push(this.updateByID(t._id, t));
            }

            return promises.push(this.addTranslation(t as CreateTranslationDTO))
        })

        return Promise.all(promises);

        // const translations = [...createTranslationDTO] as any;
        // // this.translationModel.deleteMany({}, (_) => {})
        // return await this.translationModel.updateMany({},translations, (_) => {console.log(_)})

    }

    async importAndReplace(createTranslationDTO: CreateTranslationDTO[]): Promise<Translation> {
        const translations = [...createTranslationDTO] as any;
        this.translationModel.deleteMany({}, (_) => {})
        return await this.translationModel.insertMany(translations)
    }

    async deleteTranslation(id: string): Promise<any> {
        const deletedCustomer = await this.translationModel.findByIdAndRemove(id);

        return deletedCustomer;
    }

    async updateByID(id: string, translation: Translation) {
        let success =  false;
        const entry = await this.translationModel.findByIdAndUpdate(id, translation, (err, res) => {
            success =  isNull(err)
        }).exec();

        return entry;
    }

    async filterTranslations(query: string) {
        const legendFilter = {key:query}
        const languageFilter = {language:query}
        const cellValueFilter = {string:query}
        const filterByLegend = await this.translationModel.find(legendFilter).exec();
        const filterByLanguage = await this.translationModel.find(languageFilter).exec();
        const filterByCellValue = await this.translationModel.find(cellValueFilter).exec();
        return [...filterByLegend, ...filterByLanguage, ...filterByCellValue];
    }

    async insertColumn(createTranslationDTO: InsertColumnRequestDTO): Promise<Translation> {
        const newTranslation = new this.translationModel(createTranslationDTO);
        return await newTranslation.save();
    }


    async insertRow(createTranslationDTO: InsertRowRequestDTO): Promise<Translation> {
        const newTranslation = new this.translationModel(createTranslationDTO);
        return await newTranslation.save();
    }
}
