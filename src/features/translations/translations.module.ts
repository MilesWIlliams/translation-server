import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { TranslationSchema } from './schemas/translation.schema';
import { TranslationsController } from './controllers/translations.controller';
import { TranslationsService } from './services/translations.service';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Translations', schema: TranslationSchema}])],
    controllers: [TranslationsController],
    providers: [TranslationsService],
})
export class TranslationsModule {}
